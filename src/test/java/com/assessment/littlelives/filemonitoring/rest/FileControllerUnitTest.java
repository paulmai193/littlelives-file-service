package com.assessment.littlelives.filemonitoring.rest;

import com.assessment.littlelives.filemonitoring.entity.User;
import com.assessment.littlelives.filemonitoring.rest.response.ResponseMessage;
import com.assessment.littlelives.filemonitoring.service.FileSizeQuotaService;
import com.assessment.littlelives.filemonitoring.service.UploadFileService;
import com.assessment.littlelives.filemonitoring.service.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.LimitExceededException;
import java.util.Objects;
import java.util.Optional;

class FileControllerUnitTest {

    private FileController fileController;

    @Test
    void uploadFile_exceedsQuota() {
        fileController = new FileController(
                new UserService() {
                    @Override
                    public Optional<User> findUserById(Long id) {
                        return Optional.empty();
                    }

                },
                new FileSizeQuotaService() {
                    @Override
                    public Long getQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public Long getUsage(Long userId) {
                        return null;
                    }

                    @Override
                    public Long remainingQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public boolean isAvailableQuota(Long userId, long fileSize) {
                        return false;
                    }

                    @Override
                    public void updateUsage(Long userId, long fileSize) throws Exception {

                    }

                    @Override
                    public void lockUsage(Long userId, long fileSize) {

                    }
                },
                new UploadFileService() {
                    @Override
                    public void uploadFile(MultipartFile file) {

                    }
                }
        );

        ResponseEntity<ResponseMessage> response = fileController.userUploadFile(1L, new MockMultipartFile("file", "test.txt", "text/plain", "test".getBytes()));
        Assertions.assertThat(response.getStatusCode().value()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        Assertions.assertThat(Objects.requireNonNull(response.getBody()).getMessage()).isEqualTo("Quota exceeded!");
    }

    @Test
    void uploadFile_uploadFailExceededQuota() {
        fileController = new FileController(
                new UserService() {
                    @Override
                    public Optional<User> findUserById(Long id) {
                        return Optional.empty();
                    }
                },
                new FileSizeQuotaService() {
                    @Override
                    public Long getQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public Long getUsage(Long userId) {
                        return null;
                    }

                    @Override
                    public Long remainingQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public boolean isAvailableQuota(Long userId, long fileSize) {
                        return true;
                    }

                    @Override
                    public void updateUsage(Long userId, long fileSize) throws Exception {
                        throw new LimitExceededException("Quota exceeded!");
                    }

                    @Override
                    public void lockUsage(Long userId, long fileSize) {

                    }
                },
                new UploadFileService() {
                    @Override
                    public void uploadFile(MultipartFile file) throws Exception {
                        throw new LimitExceededException("Upload failed!");
                    }
                }
        );

        ResponseEntity<ResponseMessage> response = fileController.userUploadFile(1L, new MockMultipartFile("file", "test.txt", "text/plain", "test".getBytes()));
        Assertions.assertThat(response.getStatusCode().value()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        Assertions.assertThat(Objects.requireNonNull(response.getBody()).getMessage()).isEqualTo("Quota exceeded!");
    }

    @Test
    void uploadFile_uploadFailInternalServerError() {
        fileController = new FileController(
                new UserService() {
                    @Override
                    public Optional<User> findUserById(Long id) {
                        return Optional.empty();
                    }
                },
                new FileSizeQuotaService() {
                    @Override
                    public Long getQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public Long getUsage(Long userId) {
                        return null;
                    }

                    @Override
                    public Long remainingQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public boolean isAvailableQuota(Long userId, long fileSize) {
                        return true;
                    }

                    @Override
                    public void updateUsage(Long userId, long fileSize) throws Exception {

                    }

                    @Override
                    public void lockUsage(Long userId, long fileSize) {

                    }
                },
                new UploadFileService() {
                    @Override
                    public void uploadFile(MultipartFile file) throws Exception {
                        throw new Exception("Upload failed!");
                    }
                }
        );

        ResponseEntity<ResponseMessage> response = fileController.userUploadFile(1L, new MockMultipartFile("file", "test.txt", "text/plain", "test".getBytes()));
        Assertions.assertThat(response.getStatusCode().value()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
        Assertions.assertThat(Objects.requireNonNull(response.getBody()).getMessage()).isEqualTo("Something went wrong!");
    }

    @Test
    void uploadFile_success() {
        fileController = new FileController(
                new UserService() {
                    @Override
                    public Optional<User> findUserById(Long id) {
                        return Optional.empty();
                    }
                },
                new FileSizeQuotaService() {
                    @Override
                    public Long getQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public Long getUsage(Long userId) {
                        return null;
                    }

                    @Override
                    public Long remainingQuota(Long userId) {
                        return null;
                    }

                    @Override
                    public boolean isAvailableQuota(Long userId, long fileSize) {
                        return true;
                    }

                    @Override
                    public void updateUsage(Long userId, long fileSize) throws Exception {
                        // Success
                    }

                    @Override
                    public void lockUsage(Long userId, long fileSize) {

                    }
                },
                new UploadFileService() {

                    @Override
                    public void uploadFile(MultipartFile file) throws Exception {
                        // Success
                    }
                }
        );
        ResponseEntity<ResponseMessage> response = fileController.userUploadFile(1L, new MockMultipartFile("file", "test.txt", "text/plain", "test".getBytes()));
        Assertions.assertThat(response.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
        Assertions.assertThat(Objects.requireNonNull(response.getBody()).getMessage()).isEqualTo("File uploaded successfully!");
    }
}