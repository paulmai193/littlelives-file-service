package com.assessment.littlelives.filemonitoring.service.impl;

import com.assessment.littlelives.filemonitoring.entity.User;
import com.assessment.littlelives.filemonitoring.repository.UserRepository;
import com.assessment.littlelives.filemonitoring.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findUserById(Long id) {
        return repository.findById(id);
    }
}
