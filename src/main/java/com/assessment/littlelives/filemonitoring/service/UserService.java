package com.assessment.littlelives.filemonitoring.service;

import com.assessment.littlelives.filemonitoring.entity.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findUserById(Long id);
}
