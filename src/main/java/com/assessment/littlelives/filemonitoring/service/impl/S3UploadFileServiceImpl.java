package com.assessment.littlelives.filemonitoring.service.impl;

import com.assessment.littlelives.filemonitoring.service.UploadFileService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

@Service
public class S3UploadFileServiceImpl implements UploadFileService {

    private final S3Client s3Client;
    private final String s3Bucket;

    public S3UploadFileServiceImpl(S3Client s3Client, @Qualifier("s3Bucket") String s3Bucket) {
        this.s3Client = s3Client;
        this.s3Bucket = s3Bucket;
    }

    @Override
    public void uploadFile(MultipartFile file) throws Exception {
        PutObjectRequest putOb = PutObjectRequest.builder()
                .bucket(s3Bucket)
                .key("objectKey")
                .build();

        s3Client.putObject(putOb, RequestBody.fromInputStream(file.getInputStream(), file.getSize()));
    }
}
