package com.assessment.littlelives.filemonitoring.service.impl;

import com.assessment.littlelives.filemonitoring.entity.User;
import com.assessment.littlelives.filemonitoring.repository.UserRepository;
import com.assessment.littlelives.filemonitoring.service.FileSizeQuotaService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.LimitExceededException;
import java.text.MessageFormat;

@Service
public class FileSizeQuotaServiceImpl implements FileSizeQuotaService {

    private final UserRepository userRepository;

    public FileSizeQuotaServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public Long getQuota(Long userId) {
        return getUser(userId).getQuota();
    }

    @Override
    @Transactional
    public Long getUsage(Long userId) {
        return getUser(userId).getUsage();
    }

    @Override
    @Transactional
    public Long remainingQuota(Long userId) {
        User user = getUser(userId);
        return user.getQuota() - user.getUsage() - user.getLockUsage();
    }

    @Override
    public boolean isAvailableQuota(Long userId, long fileSize) {
        return remainingQuota(userId) >= fileSize;
    }

    @Override
    @Transactional
    public void updateUsage(Long userId, long fileSize) throws Exception {
        User user = getUser(userId);
        if (user.getQuota() - user.getUsage() >= fileSize) {
            user.addUsage(fileSize);
            user.setLockUsage(user.getLockUsage() - fileSize);
            userRepository.save(user);
        } else {
            throw new LimitExceededException();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void lockUsage(Long userId, long fileSize) {
        User user = getUser(userId);
        user.addLockUsage(fileSize);
        userRepository.saveAndFlush(user);
    }

    protected User getUser(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(MessageFormat.format("User {0} not found", userId)));
    }
}
