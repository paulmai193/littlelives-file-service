package com.assessment.littlelives.filemonitoring.service;

public interface FileSizeQuotaService {

    Long getQuota(Long userId);

    Long getUsage(Long userId);

    Long remainingQuota(Long userId);

    boolean isAvailableQuota(Long userId, long fileSize);

    void updateUsage(Long userId, long fileSize) throws Exception;

    void lockUsage(Long userId, long fileSize);
}
