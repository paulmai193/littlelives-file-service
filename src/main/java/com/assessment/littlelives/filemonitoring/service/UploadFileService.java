package com.assessment.littlelives.filemonitoring.service;

import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {

    void uploadFile(MultipartFile file) throws Exception;
}
