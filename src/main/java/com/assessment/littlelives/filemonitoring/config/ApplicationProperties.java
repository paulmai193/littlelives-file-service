package com.assessment.littlelives.filemonitoring.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {

    private AWSProperties aws;

    @Data
    public static class AWSProperties {
        private String accessKey;
        private String secretKey;
        private String region;
        private String bucket;
    }
}
