package com.assessment.littlelives.filemonitoring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

@Configuration
public class S3Config {

    private final ApplicationProperties.AWSProperties awsProperties;

    public S3Config(ApplicationProperties application) {
        this.awsProperties = application.getAws();
    }

    @Bean
    public AwsCredentials awsCredentials() {
        return AwsBasicCredentials.create(awsProperties.getAccessKey(), awsProperties.getSecretKey());
    }

    @Bean
    public S3Client S3Client() {
        return S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCredentials()))
                .region(Region.of(awsProperties.getRegion()))
                .build();
    }

    @Bean(name = "s3Bucket")
    public String getBucket() {
        return awsProperties.getBucket();
    }

}
