package com.assessment.littlelives.filemonitoring.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Getter
@Setter
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "quota", nullable = false)
    private Long quota = 10000000L; // 10 MB

    @Column(name = "`usage`", nullable = false)
    private Long usage = 0L;

    @Column(name = "`lock_usage`", nullable = false)
    private Long lockUsage = 0L;

    public void addUsage(long fileSize) {
        this.usage += fileSize;
    }

    public void addLockUsage(long fileSize) {
        this.lockUsage += fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", login='").append(login).append('\'');
        sb.append(", quota=").append(quota);
        sb.append(", usage=").append(usage);
        sb.append(", lock_usage=").append(lockUsage);
        sb.append('}');
        return sb.toString();
    }

}
