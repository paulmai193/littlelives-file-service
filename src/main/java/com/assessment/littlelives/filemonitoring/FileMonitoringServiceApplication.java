package com.assessment.littlelives.filemonitoring;

import com.assessment.littlelives.filemonitoring.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class FileMonitoringServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileMonitoringServiceApplication.class, args);
	}

}
