package com.assessment.littlelives.filemonitoring.rest.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class ResponseMessage {
    private String message;
}
