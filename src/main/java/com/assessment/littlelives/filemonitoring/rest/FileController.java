package com.assessment.littlelives.filemonitoring.rest;

import com.assessment.littlelives.filemonitoring.entity.User;
import com.assessment.littlelives.filemonitoring.rest.response.ResponseMessage;
import com.assessment.littlelives.filemonitoring.service.FileSizeQuotaService;
import com.assessment.littlelives.filemonitoring.service.UploadFileService;
import com.assessment.littlelives.filemonitoring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.LimitExceededException;
import java.util.Optional;

@RestController
@RequestMapping(path="/api/v1", produces="application/json")
@Slf4j
public class FileController {

    private final UserService userService;
    private final FileSizeQuotaService fileSizeQuotaService;
    private final UploadFileService uploadFileService;

    public FileController(UserService userService, FileSizeQuotaService fileSizeQuotaService, UploadFileService uploadFileService) {
        this.userService = userService;
        this.fileSizeQuotaService = fileSizeQuotaService;
        this.uploadFileService = uploadFileService;
    }

    @PostMapping("/user/{userId}/upload")
    public ResponseEntity<ResponseMessage> userUploadFile(@PathVariable Long userId, @RequestParam("file") MultipartFile file) {
        log.info("REST request upload file(s) from user {}", userId);
        if (fileSizeQuotaService.isAvailableQuota(userId, file.getSize())) {
            try {
                fileSizeQuotaService.lockUsage(userId, file.getSize()); // lock usage to deal with concurrent update
                uploadFileService.uploadFile(file);
                fileSizeQuotaService.updateUsage(userId, file.getSize());
                return ResponseEntity.ok(new ResponseMessage("File uploaded successfully!"));
            }
            catch (LimitExceededException e) {
                return ResponseEntity.badRequest().body(new ResponseMessage("Quota exceeded!"));
            }
            catch (Exception e) {
                return ResponseEntity.internalServerError().body(new ResponseMessage("Something went wrong!"));
            }
        }
        else {
            return ResponseEntity.badRequest().body(new ResponseMessage("Quota exceeded!"));
        }
    }
}
